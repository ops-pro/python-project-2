Домашние задания:
-------
1. создать pvc
2. подключить pvc аналогично тому, как было в docker-compose.yml

KUBERNETES:
-------

commands:
Создать секрет из локального конфига для докера.
```bash
kubectl create secret generic regcred --from-file=.dockerconfigjson=/root/.docker/config.json --type=kubernetes.io/dockerconfigjson
```

Удалить секрет, если уже есть (или просто хотим стереть свои учетные данные):
```bash
kubectl delete secret regcred
```

MY SUPER APP
------------------

## Local Installation

It's easy to install and run it on your computer.

```shell
# 1. First, clone the repo
$ git clone
$ cd keras-flask-deploy-webapp

# 2. Install Python packages
$ pip install -r requirements.txt

# 3. Run!
$ python app.py
```

Open http://localhost:5000 and have fun. :smiley:
